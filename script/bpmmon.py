#!/usr/bin/python3
import epics
import numpy as np
import threading
import sys
import time
import os
from gfit import *

def main():
    ## load env variables
    if "DEVBPM" in os.environ:
        N = os.environ["DEVBPM"]
    else:
        N = "1"
    #print("Script using prefix BPM:{}".format(N))

    if "DEVCAMERA" in os.environ:
        PG = os.environ["DEVCAMERA"]
    else:
        PG = "PG01"
    #print("Script using prefix CAMERA:{}".format(PG))

    ## constants
    DEBUG=False

    ## Loop lock to sync with PV data
    global eve
    eve = threading.Event()

    ## epics PVs
    profileX_pvname = "PINK:{}:Stats2:ProfileAverageX_RBV".format(PG)
    profileY_pvname = "PINK:{}:Stats2:ProfileAverageY_RBV".format(PG)
    centerX_pvname = "PINK:BPM:{}:X:center".format(N)
    centerY_pvname = "PINK:BPM:{}:Y:center".format(N)
    centerX_mass_pvname = "PINK:BPM:{}:X:center_mass".format(N)
    centerY_mass_pvname = "PINK:BPM:{}:Y:center_mass".format(N)
    valid_pvname = "PINK:BPM:{}:valid".format(N)
    enable_pvname = "PINK:BPM:{}:enable".format(N)
    plot_enable_pvname = "PINK:BPM:{}:plot_enable".format(N)
    fwhmX_pvname = "PINK:BPM:{}:X:fwhm".format(N)
    fwhmY_pvname = "PINK:BPM:{}:Y:fwhm".format(N)
    fitarrayX_pvname = "PINK:BPM:{}:X:ydata".format(N)
    fitarrayY_pvname = "PINK:BPM:{}:Y:ydata".format(N)
    roiposX_pvname = "PINK:{}:ROI1:MinX_RBV".format(PG)
    roiposY_pvname = "PINK:{}:ROI1:MinY_RBV".format(PG)
    resolutionY_pvname = "PINK:BPM:{}:Y:resolution".format(N)
    resolutionX_pvname = "PINK:BPM:{}:X:resolution".format(N)
    angleY_pvname = "PINK:BPM:{}:Y:angle_correction".format(N)
    angleX_pvname = "PINK:BPM:{}:X:angle_correction".format(N)
    centerX_mass_diff_pvname = "PINK:BPM:{}:X:cmass_diff".format(N)
    centerY_mass_diff_pvname = "PINK:BPM:{}:Y:cmass_diff".format(N)

    ## callback function
    def onValueChange(pvname=None, value=None, host=None, **kws):
        global eve
        eve.set()

    ## create PV channels
    print("Creating PV channels...")
    profileX = epics.PV(profileX_pvname, auto_monitor=True, callback=onValueChange)
    profileY = epics.PV(profileY_pvname, auto_monitor=True)
    centerX = epics.PV(centerX_pvname, auto_monitor=False)
    centerY = epics.PV(centerY_pvname, auto_monitor=False)
    centerX_mass = epics.PV(centerX_mass_pvname, auto_monitor=False)
    centerY_mass = epics.PV(centerY_mass_pvname, auto_monitor=False)
    valid = epics.PV(valid_pvname, auto_monitor=False)
    enable = epics.PV(enable_pvname, auto_monitor=True)
    plot_enable = epics.PV(plot_enable_pvname, auto_monitor=True)
    fwhmX = epics.PV(fwhmX_pvname, auto_monitor=False)
    fwhmY = epics.PV(fwhmY_pvname, auto_monitor=False)
    fitarrayX = epics.PV(fitarrayX_pvname, auto_monitor=False)
    fitarrayY = epics.PV(fitarrayY_pvname, auto_monitor=False)
    roiposX = epics.PV(roiposX_pvname, auto_monitor=True)
    roiposY = epics.PV(roiposY_pvname, auto_monitor=True)
    resolutionY = epics.PV(resolutionY_pvname, auto_monitor=True)
    resolutionX = epics.PV(resolutionX_pvname, auto_monitor=True)
    angleY = epics.PV(angleY_pvname, auto_monitor=True)
    angleX = epics.PV(angleX_pvname, auto_monitor=True)
    centerX_mass_diff = epics.PV(centerX_mass_diff_pvname, auto_monitor=False)
    centerY_mass_diff = epics.PV(centerY_mass_diff_pvname, auto_monitor=False)

    time.sleep(2)

    ## dictionary structure
    Xfit = {
        "valid" : False,
        "snr" : 0.0,
        "center" : 0,
        "center_m" : 0,
        "cmass_diff" : 0,
        "fwhm" : 0.0,
        "fitarray" : []
    }
    Yfit = Xfit.copy()

    ## Main loop
    print("BPM Monitor has started:")
    print("BPM{} with {}".format(N,PG))
    print("[{}] BPMMON is running ...".format(time.asctime()))
    while(True):
        try:
            eve.wait()
            t1=time.time()
            if enable.value>0:
                gfit(profileX.value, Xfit)
                gfit(profileY.value, Yfit)
                isvalid = Xfit["valid"]
                valid.put(isvalid)
                if isvalid:
                    centerX.put(Xfit["center"]+roiposX.value)
                    centerY.put(Yfit["center"]+roiposY.value)
                    centerX_mass.put(Xfit["center_m"]+roiposX.value)
                    centerY_mass.put(Yfit["center_m"]+roiposY.value)
                    fwhmX.put(Xfit["fwhm"]*resolutionX.value*angleX.value)
                    fwhmY.put(Yfit["fwhm"]*resolutionY.value*angleY.value)
                    centerX_mass_diff.put(Xfit["cmass_diff"]*resolutionX.value*angleX.value)
                    centerY_mass_diff.put(Yfit["cmass_diff"]*resolutionY.value*angleY.value)
                    if plot_enable.value>0:
                        fitarrayX.put(Xfit["fitarray"])
                        fitarrayY.put(Yfit["fitarray"])
            else:
                valid.put(0)
            t2=time.time()
            if DEBUG: print('dt: {:.3f} ms\n'.format((t2-t1)*1000.0))
        except Exception as err:
            print("[{}] Catched exception:".format(time.asctime()))
            print(err)
        eve.clear()

main()
print("OK")

