import numpy as np
from scipy.optimize import curve_fit
import time

def gfit(ydata, outdata):
    ## constants
    DEBUG=False
    SNR_threshold = 10.0
    FWHM_factor = 2.355

    t1=time.time()

    ## Gaussian equation with linear background
    def func(x, a, b, c, d, e):
        return a*x + b + c * np.exp(-(np.power((x - d), 2) / (2 * np.power(e, 2))))

    ## some parameters for fitting
    ydim=len(ydata)
    xdata = np.linspace(0,ydim-1,ydim)
    bg_slope = 0
    bg_value = np.min(ydata)
    height = np.max(ydata)
    center = np.argmax(ydata)

    snr = 100*((height/np.mean(ydata))-1)
    outdata["snr"]=snr
    if DEBUG: print('snr: {:.1f} %'.format(snr))

    ## try to identified a valid profile
    if SNR_threshold>snr:
        outdata["valid"]=False
        return

    ## more parameters for fitting
    ycut=(height-bg_value)/2+bg_value
    ycutmask=(ydata>ycut)
    y1=np.multiply(ydata,ycutmask)
    y2=np.diff(y1)
    sigma_guess=(np.argmin(y2)-np.argmax(y2))/2
    p0=[bg_slope, bg_value, height, center, sigma_guess]

    ## fitting
    popt, pcov = curve_fit(func, xdata, ydata, p0=p0)

    ## center of mass calc
    center = popt[3]
    sigma = popt[4]
    # limit domain to 3.sigma to each side of the gaussian center
    x0 = int(np.max([0, np.floor(center-3*sigma)]))
    x1 = int(np.min([ydim-1, np.ceil(center+3*sigma)]))
    ycm = ydata[x0:x1]
    xcm = xdata[x0:x1]
    ycm = np.subtract(ycm, np.min(ycm))
    cmass = np.sum(np.multiply(xcm,ycm))/np.sum(ycm)
    cmassdiff = cmass-center
    if DEBUG: print("Cmass_dif: {:.9f}".format(cmassdiff))

    ## Return values
    outdata["valid"]=True
    outdata["center"] = center
    outdata["center_m"] = cmass
    outdata["fwhm"] = sigma*FWHM_factor
    outdata["fitarray"] = func(xdata, *popt)
    outdata["cmass_diff"] = cmassdiff

    t2=time.time()
    if DEBUG: print('dt: {:.1f} ms\n'.format((t2-t1)*1000.0))
    return
